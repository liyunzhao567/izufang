# 加载Django项目配置
import os

import celery
from celery.schedules import crontab
from django.conf import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'izufang.settings')

# 创建Celery对象，指定模块名、消息代理（消息队列服务）
app = celery.Celery(
    main='izufang',
    broker='redis://:Luohao.618@47.104.31.138:5489/1'
)

# # 从配置文件中读取Celery相关配置
# app.config_from_object('django.conf:settings')
# 自动从指定的应用中发现任务（异步任务/定时任务）
# app.autodiscover_tasks(('common', ))
# 自动从注册的应用中发现任务（异步任务/定时任务）
app.autodiscover_tasks(settings.INSTALLED_APPS)

app.conf.update(
    timezone=settings.TIME_ZONE,
    enable_utc=True,
    # 定时任务（计划任务）相当于是消息的生产者
    # celery -A izufang beat -l debug
    # 如果只有生产者没有消费者那么消息就会在消息队列中积压
    # celery -A izufang worker -l debug
    # 将来实际部署项目的时候生产者、消费者、消息队列可能都是不同节点
    beat_schedule={
        'task1': {
            'task': 'common.tasks.check_inactive_users',
            'schedule': crontab(minute='20', hour='21'),
        },
    },
)
