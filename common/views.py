from django.http import HttpResponse
from django.shortcuts import redirect

from common.captcha import Captcha
from common.utils import gen_qrcode, gen_captcha_text


def show_index(request):
    return redirect('/static/html/index.html')


def show_qrcode(request):
    data = gen_qrcode('https://blog.csdn.net/jackfrued')
    return HttpResponse(data, content_type='image/png')


def get_captcha(request):
    instance = Captcha.instance()
    text = gen_captcha_text()
    data = instance.generate(text)
    return HttpResponse(data, content_type='image/png')
